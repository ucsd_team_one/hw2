#ifndef UTILS_H
#define UTILS_H

#include "mbed.h"

// sepcial characters
const char _CLS_[] = "\x1B[2J";     // VT100 erase screen
const char _HOME_[] = "\x1B[H";     // VT100 home

int float_send(float val, Serial &s);
float float_recv(Serial &s);

#endif