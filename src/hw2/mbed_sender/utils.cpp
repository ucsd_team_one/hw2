#include "utils.h"

int float_send(float val, Serial &s) {
    // init buffer
    char buffer[5];
    // write to buffer
    memcpy(buffer, &val, sizeof(float));
    // write string end character
    buffer[4] = '\0';
    // send
    for (int i = 0; i < 4; i++) {
        s.putc(buffer[i]);
    }
    // send endl for gets
    s.putc('\n');
    
    return 0;
}

float float_recv(Serial &s) {
    // init buffer
    char buffer[5];
    // receive
    s.gets(buffer, 5);
    // write to float
    float val;
    memcpy(&val, buffer, sizeof(float));
    
    return val;
}