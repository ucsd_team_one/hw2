#include "mbed.h"
#include "utils.h"
#include <string.h>

DigitalOut myled(LED1);
Serial pc(USBTX, USBRX);
AnalogIn mypot(p20);
Serial uart(p28, p27); // T, R

int main() {
    myled = 1;
    while(1) {
        myled = 1 - myled;
        float voltage = mypot;
        float_send(voltage, pc);
//        float val;
//        val = float_recv(uart);
//        pc.printf(_CLS_);
//        pc.printf(_HOME_);
//        pc.printf("%f\t", val);
        wait(0.4);
    }
}