/*
 * rosserial Publisher Example
 * Prints "hello world!"
 */

#include <ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Float32.h>
#include<Arduino.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
// Or, create it with a different I2C address (say for stacking)
// Adafruit_MotorShield AFMS = Adafruit_MotorShield(0x61); 

// Select which 'port' M1, M2, M3 or M4. In this case, M1
Adafruit_DCMotor *myMotor = AFMS.getMotor(1);
Adafruit_DCMotor *myMotor2 = AFMS.getMotor(2);

ros::NodeHandle nh;

std_msgs::String str_msg, arm_cmd;
std_msgs::Float32 ang_msg;
ros::Publisher chatter("chatter", &str_msg);
ros::Publisher ard_angle_pub("ard_angle", &ang_msg);


char hello[13] = "hello world!";
char forward[8] = "forward";
char back[5] = "back";
char error[6] = "orrar";
char gotit[19] = "got angle";
char w[2] = "w";
char s[2] = "s";
float a = 40.0;
float b = 60.0;
float angle = 0.0;

void angle_callback(const std_msgs::Float32& angle_msg){
  angle = angle_msg.data;
  str_msg.data = gotit;
  chatter.publish(&str_msg);
  ang_msg.data = angle;
  ard_angle_pub.publish(&ang_msg);
}
void callback(const std_msgs::String& msg){
  uint8_t i;
  String msgdata(msg.data);
//  String w("w");
//  String s("s");
  arm_cmd.data = msg.data;
  if (strcmp(msg.data, "w") == 0){
    arm_cmd.data = forward;
    ////run the second motor
    myMotor2->run(FORWARD);
    myMotor2->setSpeed(100);
//    delay(50);
    while (angle > a){
//      chatter.publish("%4.2f", angle); 
      delay(5);
    }
    myMotor2->run(RELEASE);
  } else if (strcmp(msg.data, "s") == 0) {
    arm_cmd.data = back;
    myMotor2->run(BACKWARD);
    myMotor2->setSpeed(100);
//    delay(50);
    while (angle < b) {
//      chatter.publish("%4.2f", angle); 
      delay(5);
    }
    myMotor2->run(RELEASE);
//    delay(50);ssssssssss
  }
  myMotor2->run(RELEASE);
  chatter.publish(&arm_cmd);
  
}

ros::Subscriber<std_msgs::String> cmds("arm_cmds", &callback);
ros::Subscriber<std_msgs::Float32> angle_sub("arm_angle", &angle_callback);
void setup()
{
  nh.initNode();
  nh.advertise(chatter);
  nh.advertise(ard_angle_pub);
  nh.subscribe(cmds);
  nh.subscribe(angle_sub);
    Serial.begin(57600);           // set up Serial library at 9600 bps
  Serial.println("Adafruit Motorshield v2 - DC Motor test!");

  AFMS.begin();  // create with the default frequency 1.6KHz
  //AFMS.begin(1000);  // OR with a different frequency, say 1KHz
  
  // Set the speed to start, from 0 (off) to 255 (max speed)
  myMotor->setSpeed(20);
  myMotor2->setSpeed(20);
  myMotor->run(FORWARD);
  // turn on motor
  myMotor->run(RELEASE);
  myMotor2->run(FORWARD);
  // turn on motor
  myMotor2->run(RELEASE);
}

void loop()
{
//  str_msg.data = to_string(angle);
//  chatter.publish( &str_msg );
  nh.spinOnce();
  delay(1000);
}
